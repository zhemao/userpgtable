#include <sys/mman.h>
#include <sys/fcntl.h>
#include <sys/types.h>
#include <syscall.h>
#include <unistd.h>

#define __NR_expose_page_table 376

#define PAGE_TABLE_SIZE (4*1024*1024) /* 4mb */
#define PAGE_SHIFT 12
#define PTE_MASK ~((1 << PAGE_SHIFT) - 1)

/* Some helpful macros */
#define pg_num_to_idx(num) (((num / 512 * 4096) / 4) + (num % 512))
#define get_page(page_table, num) \
		page_table[pg_num_to_idx(num)]
#define page_present(page) ((page) & 1)
#define page_young(page) (((page) >> 1) & 1)
#define page_file(page) (((page) >> 2) & 1)
#define page_dirty(page) (((page) >> 6) & 1)
#define page_rdonly(page) (((page) >> 7) & 1)
#define page_user(page) (((page) >> 8) & 1)
#define pgnum_to_addr(pgnum) ((pgnum) << PAGE_SHIFT)

/* Wrapper around expose_page_table() system call - do not modify! */
static unsigned int *expose_page_table()
{
	int i, fault;
	int fd = open("/dev/zero", O_RDONLY);
	unsigned int *addr = mmap(NULL, PAGE_TABLE_SIZE * 2, PROT_READ,
				 MAP_SHARED, fd, 0);
	close(fd);
	if (addr == MAP_FAILED) {
		perror("mmap failed");
		return NULL;
	}
	/* Fault now to avoid handling in kernel */
	for (i = 0; i < (PAGE_TABLE_SIZE * 2) / 4; ++i)
		fault = addr[i];
	if (syscall(__NR_expose_page_table, addr) < 0) {
		perror("expose_page_table syscall failed");
		return NULL;
	}
	return addr;
}
