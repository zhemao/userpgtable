#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "pgtable.h"

static void print_page(int pgnum, unsigned int page)
{
	unsigned int virt_addr, phys_addr;

	virt_addr = pgnum_to_addr(pgnum);
	phys_addr = page & PTE_MASK;

	printf("%d %x %x %d %d %d %d %d\n", pgnum, virt_addr, phys_addr,
		page_young(page), page_file(page), page_dirty(page),
		page_rdonly(page), page_user(page));
}

int print_page_table()
{
	unsigned int *page_table;
	unsigned int page;
	int pgnum;

	page_table = expose_page_table();

	if (page_table == NULL)
		return EXIT_FAILURE;

	for (pgnum = 0; pgnum < PAGE_TABLE_SIZE / 4; pgnum++) {
		page = get_page(page_table, pgnum);

		if (page && page_present(page))
			print_page(pgnum, page);
	}

	/* munmap(page_table, 2 * PAGE_TABLE_SIZE); */

	return EXIT_SUCCESS;
}

/* Print the process' page table */
int main(int argc, char **argv)
{
	if (print_page_table() == EXIT_FAILURE)
		return EXIT_FAILURE;
	if (print_page_table() == EXIT_FAILURE)
		return EXIT_FAILURE;

	return EXIT_SUCCESS;
}
