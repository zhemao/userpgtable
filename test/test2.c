#define _GNU_SOURCE
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "pgtable.h"
#include "obscured.h"

static unsigned int *duplicate_page_table(unsigned int *page_table)
{
	unsigned int *pgt_copy;

	pgt_copy = malloc(2 * PAGE_TABLE_SIZE);

	if (pgt_copy == NULL)
		return NULL;

	memcpy(pgt_copy, page_table, 2 * PAGE_TABLE_SIZE);

	return pgt_copy;
}

static int
page_modified(unsigned int *old_table, unsigned int *page_table, int pgnum)
{
	unsigned int old_page, new_page;

	old_page = get_page(old_table, pgnum);
	new_page = get_page(page_table, pgnum);

	if (new_page && page_present(new_page)) {
		if (!old_page || !page_present(old_page))
			return 1;
		if (!page_dirty(old_page) && page_dirty(new_page))
			return 1;
	}

	return 0;
}

static int find_secret_msg(unsigned int *old_table, unsigned int *page_table)
{
	int i;
	void *addr;

	for (i = 0; i < PAGE_TABLE_SIZE / 4; i++) {
		if (!page_modified(old_table, page_table, i))
			continue;

		addr = (void *) pgnum_to_addr(i);

		if (super_double_secret_msg_decoder(addr) == 0)
			return 0;
	}

	return -1;
}

static int pgtables_same(unsigned int *page_table1, unsigned int *page_table2)
{
	int i;

	for (i = 0; i < PAGE_TABLE_SIZE / 4; i++) {
		if (get_page(page_table1, i) != get_page(page_table2, i)) {
			printf("page tables differ at %x\n", pgnum_to_addr(i));
			return 0;
		}
	}

	return 1;
}

/* Find the super secret secret message */
int main(int argc, char **argv)
{
	/* Obtain page table mapping */
	unsigned int *page_table1, *page_table2, *old_table;
	int retval;

	page_table1 = expose_page_table();

	if (page_table1 == NULL)
		return EXIT_FAILURE;

	page_table2 = expose_page_table();

	if (page_table2 == NULL) {
		munmap(page_table1, 2 * PAGE_TABLE_SIZE);
		return EXIT_FAILURE;
	}

	if (!pgtables_same(page_table1, page_table2)) {
		munmap(page_table1, 2 * PAGE_TABLE_SIZE);
		munmap(page_table2, 2 * PAGE_TABLE_SIZE);
		return EXIT_FAILURE;
	}

	old_table = duplicate_page_table(page_table1);

	if (old_table == NULL) {
		munmap(page_table1, 2 * PAGE_TABLE_SIZE);
		munmap(page_table2, 2 * PAGE_TABLE_SIZE);
		return EXIT_FAILURE;
	}

	store_super_double_secret_msg();

	if (!pgtables_same(page_table1, page_table2)) {
		munmap(page_table1, 2 * PAGE_TABLE_SIZE);
		munmap(page_table2, 2 * PAGE_TABLE_SIZE);
		free(old_table);
		return EXIT_FAILURE;
	}

	/* Identify modified pages here */

	/* Try to decode modified pages here
	 * super_double_secret_msg_decoder();
	 */

	retval = find_secret_msg(old_table, page_table2);

	munmap(page_table1, 2 * PAGE_TABLE_SIZE);
	munmap(page_table2, 2 * PAGE_TABLE_SIZE);
	free(old_table);

	return retval;
}
