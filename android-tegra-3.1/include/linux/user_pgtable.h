#ifndef _LINUX_USER_PGTABLE_H
#define _LINUX_USER_PGTABLE_H

int expose_pte(struct mm_struct *mm, pmd_t *pmd,
		unsigned long addr, unsigned long useraddr);

int unexpose_pte(struct mm_struct *mm, pgtable_t pte, unsigned long useraddr);

#endif
