#include <linux/syscalls.h>
#include <linux/slab.h>
#include <linux/mm_types.h>
#include <linux/mm.h>
#include <linux/io.h>
#include <linux/uaccess.h>

/* the reason we divide by PAGE_SIZE and then multiply by it is to
 * truncate the less significant bits */
#define addr_to_pte_addr(addr) ((addr) / PAGE_SIZE / PTRS_PER_PTE * PAGE_SIZE)

static const int table_size = PTRS_PER_PGD * PTRS_PER_PUD *
			PTRS_PER_PMD * PAGE_SIZE;

int unexpose_pte(struct mm_struct *mm, pgtable_t pte, unsigned long useraddr)
{
	unsigned long physaddr, mapaddr;
	struct vm_area_struct *vma;

	physaddr = page_to_phys(pte);
	mapaddr = useraddr + addr_to_pte_addr(physaddr);

	vma = find_vma(mm, mapaddr);

	if (vma == NULL) {
		pr_err("Cannot find vma.\n");
		return -1;
	}

	pr_info("unexposed %lx at %lx\n", physaddr, mapaddr);

	return zap_page_range(vma, mapaddr, PAGE_SIZE, NULL);
}

/* remaps a pte page into user memory
 * @mm: mm_struct of the process we're mapping into
 * @pmd: the pmd entry pointing to the pte page
 * @addr: the kernel address of the first page pointed to by the pte
 * @useraddr: the address of the beginning of the user-space page table
 *
 * Note - must call down_write on mm->mmap_sem before calling this function */
int expose_pte(struct mm_struct *mm, pmd_t *pmd,
	       unsigned long addr, unsigned long useraddr)
{
	pte_t *pte;
	int retval;
	unsigned long mapaddr, physaddr;
	struct vm_area_struct *vma;

	pte = pte_offset_map(pmd, addr);

	if (pte == NULL) {
		pr_err("Cannot find pte.\n");
		return -1;
	}

	physaddr = virt_to_phys(pte) >> PAGE_SHIFT;
	mapaddr = useraddr + addr_to_pte_addr(addr);

	vma = find_vma(mm, mapaddr);

	if (vma == NULL) {
		pr_err("Cannot find vma.\n");
		return -1;
	}

	pr_info("exposed %lx to %lx\n", addr, mapaddr);

	retval = remap_pfn_range(vma, mapaddr, physaddr,
				 PAGE_SIZE, PAGE_READONLY);

	pte_unmap(pte);

	if (retval < 0) {
		pr_err("remap returns error code %d\n", retval);
		return -1;
	}

	return 0;
}

static int expose_pmd(struct mm_struct *mm, pud_t *pud,
		      unsigned long addr, unsigned long end,
		      unsigned long useraddr)
{
	unsigned long next;
	pmd_t *pmd;

	pmd = pmd_offset(pud, addr);

	do {
		next = pmd_addr_end(addr, end);

		if (pmd_none_or_clear_bad(pmd))
			continue;

		if (expose_pte(mm, pmd, addr, useraddr))
			return -1;
	} while (pmd++, addr = next, addr != end);

	return 0;
}

static int expose_pud(struct mm_struct *mm, pgd_t *pgd,
		      unsigned long addr, unsigned long end,
		      unsigned long useraddr)
{
	unsigned long next;
	pud_t *pud;

	pud = pud_offset(pgd, addr);

	do {
		next = pud_addr_end(addr, end);

		if (pud_none_or_clear_bad(pud))
			continue;

		if (expose_pmd(mm, pud, addr, next, useraddr))
			return -1;
	} while (pud++, addr = next, addr != end);

	return 0;
}

static int expose_pgd(struct mm_struct *mm, struct vm_area_struct *vma,
		      unsigned long useraddr)
{
	unsigned long addr = vma->vm_start;
	unsigned long end = vma->vm_end;
	unsigned long next;
	pgd_t *pgd;

	pgd = pgd_offset(mm, addr);

	do {
		next = pgd_addr_end(addr, end);
		if (pgd_none_or_clear_bad(pgd))
			continue;

		if (expose_pud(mm, pgd, addr, next, useraddr))
			return -1;
	} while (pgd++, addr = next, addr != end);

	return 0;
}

static int expose_mm(struct mm_struct *mm, unsigned long useraddr)
{
	struct vm_area_struct *vma;
	struct mm_user_pgt *pgt;
	long diff;

	for (vma = mm->mmap; vma; vma = vma->vm_next) {
		if (expose_pgd(mm, vma, useraddr))
			return -EFAULT;
	}

	read_lock(&mm->user_pgt_lock);
	list_for_each_entry(pgt, &mm->user_pgt_list, pgt_list) {
		diff = useraddr - pgt->user_pgt_base;
		if (diff < 0)
			diff = -diff;
		if (diff < table_size) {
			read_unlock(&mm->user_pgt_lock);
			return -EINVAL;
		}
	}
	read_unlock(&mm->user_pgt_lock);

	pgt = kmalloc(sizeof(struct mm_user_pgt), GFP_KERNEL);
	if (!pgt)
		return -ENOMEM;
	pgt->user_pgt_base = useraddr;

	write_lock(&mm->user_pgt_lock);
	list_add_tail(&pgt->pgt_list, &mm->user_pgt_list);
	write_unlock(&mm->user_pgt_lock);

	return 0;
}

SYSCALL_DEFINE1(expose_page_table, void __user *, addr)
{
	struct mm_struct *mm = current->mm;
	int retval;

	if (addr == NULL)
		return -EINVAL;

	if (!access_ok(VERIFY_WRITE, addr, table_size)) {
		pr_err("Cannot access userspace");
		return -EFAULT;
	}

	down_write(&mm->mmap_sem);
	retval = expose_mm(mm, (unsigned long) addr);
	up_write(&mm->mmap_sem);

	return retval;
}
